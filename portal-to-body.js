class PortalToBody extends HTMLElement {  
    constructor() {
      super();
    }
  
    connectedCallback() {
      if (this.shadowRoot) {
        return;
      }
      const shadow = this.attachShadow({ mode: "open" });
      shadow.innerHTML = `<p>Portal to body</p><slot></slot>`;
      document.body.append(this);
      this.setAttribute("mounted", "true");
    }
  
    disconnectedCallback() {
      if (this.hasAttribute("mounted")) {
        document.body.removeChild(this);
      }
    }
  }
  
  customElements.define("portal-to-body", PortalToBody);
  