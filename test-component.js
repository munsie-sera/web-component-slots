class TestComponent extends HTMLElement {  
    constructor() {
      super();
    }
  
    connectedCallback() {
      const shadow = this.attachShadow({ mode: "open" });
      shadow.innerHTML = `
        <portal-to-body>  
          <slot></slot>
        </portal-to-body>
      `;
    }
}
 
customElements.define("test-component", TestComponent);
  